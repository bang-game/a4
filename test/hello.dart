class PubScpec {
  String version;
  List<String> _dependencies;

  List<String> get dependencies => _dependencies;

  set dependencies(List<String> value) {
    _dependencies = value;
  }


}

main() {
  var pubScpec = PubScpec();
  pubScpec.version = "1";
  pubScpec._dependencies = ["one", "deux"];

  print(pubScpec._dependencies);
  print(pubScpec.version);
}
